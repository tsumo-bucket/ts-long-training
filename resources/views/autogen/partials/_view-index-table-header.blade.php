<?php $i = 0; ?>

@if($table == 'users')
    <th class="caboodle-table-col-header hide">Details</th>
    <th class="caboodle-table-col-header hide">Type</th>
@else
    @foreach ($columns as $c)
        <?php 
            $column = str_replace('_', ' ', ucfirst($c));
        ?>
        @if ($i > 0)
        <th class="caboodle-table-col-header hide" >{{$column}}</th>
        @else
        <th class="caboodle-table-col-header hide" >{{$column}}</th>
        @endif
        <?php $i++; ?>
    @endforeach
@endif
