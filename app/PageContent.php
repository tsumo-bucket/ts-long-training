<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PageContent extends BaseModel
{
    
    protected $fillable = [
    	'page_id',

        'reference_id',

        'reference_type',

        'name',

        'slug',

        'title',

        'description',

        'bg_image',

        'published',

        'order',

        'enabled',

        'editable',

        'allow_add_items',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }
    public function pageGroup() {
        return $this->belongsTo('App\Page', 'page_id');
    }

    public function bgImage() {
        return $this->hasOne('App\Asset', 'id', 'bg_image');
    }

    public function items() {
        return $this->hasMany('App\PageContentItem', 'content_id')->where('slug', '!=', 'template-default')->orderBy('order');
	}
	
	public function publishedItems() {
        return $this->hasMany('App\PageContentItem', 'content_id')->with('bgImage', 'asset')->where('published', 'published');
    }
    public function controls()
    {
        return $this->morphMany('App\PageControl', 'reference')->orderBy('order');
    }

    public function clientItems(){
        return $this->hasMany('App\PageContentItem', 'content_id')->where('slug', '!=', 'template-default')->where('enabled','1')->orderBy('order');
    }
}
