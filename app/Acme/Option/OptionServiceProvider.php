<?php

namespace App\Acme\Option;

use Illuminate\Support\ServiceProvider;

class OptionServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('option', 'App\Acme\Option\Option');
	}
}